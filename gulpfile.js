var gulp = require('gulp');
var sass = require('gulp-sass'),
browsersync=require('browser-sync').create(),
autoprefixer=require('gulp-autoprefixer');

gulp.task('browser-sync', function() {
	browsersync.init({
		server: {
			baseDir: "./"
		},
		open:false
	});
});
gulp.task('sass', function () {
	return gulp.src('./src/styles/main.scss')
	.pipe(sass().on('error', sass.logError))
	.pipe(autoprefixer({
					browsers: ['> 0%'],
			cascade: false
	}))
	.pipe(gulp.dest('./dist/css'))
	
});

gulp.task('sass:watch',['browser-sync'], function () {
	
	gulp.watch('./src/styles/**/*.scss', ['sass'],browsersync.reload);
});
gulp.task('default',['sass','sass:watch']);